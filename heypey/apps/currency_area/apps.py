from django.apps import AppConfig


class CurrencyConfig(AppConfig):
    name = "currency_area"
    icon = '<i class="material-icons">loyalty</i>'
    verbose_name = 'Zone monetaire'