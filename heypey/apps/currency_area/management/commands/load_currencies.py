import json
import urllib2
from django.core.management import BaseCommand as NoArgsCommand
from django.core.management import CommandError

from currency_area.models import Currency

CURRENCIES_URL = "http://openexchangerates.org/currencies.json"


class Command(NoArgsCommand):
        help = "Load currencies from %s" % CURRENCIES_URL

        def handle(self, **options):

            f = urllib2.urlopen(CURRENCIES_URL)
            currencies = json.loads(f.read())

            for code, name in currencies.iteritems():
                try:
                    Currency.objects.get(code=code)
                except Currency.DoesNotExist:
                    Currency.objects.create(code=code, name=name)
