# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=3, verbose_name='Code')),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
                ('symbol', models.CharField(max_length=1, null=True, verbose_name='Symbol', blank=True)),
                ('is_default', models.BooleanField(default=False, help_text='Make this the default currency.', verbose_name='Default')),
            ],
            options={
                'ordering': ('code',),
                'verbose_name': 'Currency area',
                'verbose_name_plural': 'Currency areas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExchangeRate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Date')),
                ('rate', models.DecimalField(verbose_name='Rate', max_digits=12, decimal_places=6)),
                ('created', models.DateTimeField(auto_now=True, verbose_name='Created')),
                ('currency', models.ForeignKey(related_name='rates', to='currency_area.Currency')),
            ],
            options={
                'ordering': ('-date', 'currency__code'),
                'verbose_name': 'Exchange rate',
                'verbose_name_plural': 'Exchange rates',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='exchangerate',
            unique_together=set([('currency', 'date')]),
        ),
    ]
