# vim: set fileencoding=utf-8 :
from django.contrib import admin
from django.contrib.gis import admin as gis_admin
from django.contrib.gis.db import models as gismodels
from django.conf import settings
from olwidget.admin import GeoModelAdmin
from . import models





class CountryAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'code',
        'codeISO3',
        'name',
        'currency_area',
        'status',
    )
    list_filter = ('currency_area', 'status')
    search_fields = ('name',)


class DecoupageAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'brand_name',
        'country',
        'decoupage_type',
        'status',
        'parent',
        u'lft',
        u'rght',
        u'tree_id',
        u'level',
    )
    list_filter = ('country', 'status', 'parent')

   


class ReleveZonalAdmin(gis_admin.OSMGeoAdmin):
   
    default_lat = 1654650.17223
    default_lon = -1944593.09652
    default_zoom = 13
    map_width = 1000
    map_height = 400

  
    list_display = (
        u'id',
        'zone',
       
       
    )
    list_filter = ('zone',)
    list_select_related = (
        'zone',
    )


class ZoneListAdmin(GeoModelAdmin,gis_admin.OSMGeoAdmin):

    list_display = (
       
    )
 
    options = {
        'layers': ['google.streets'],
        'default_lat':  1654650.17223,
        'default_lon': --1944593.09652,
        'map_width' : 2000,
        'map_height' : 800,
    }

    list_map = ['mpoly']

  
def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Country, CountryAdmin)
_register(models.Decoupage, DecoupageAdmin)
_register(models.ReleveZonal, ReleveZonalAdmin)
_register(models.ZoneList, ZoneListAdmin)