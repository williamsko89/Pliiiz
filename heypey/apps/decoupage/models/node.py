#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2016 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"



from mptt.models import MPTTModel, TreeForeignKey
from django_countries.fields import CountryField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from currency_area.models import Currency
from sorl.thumbnail import ImageField
from django.conf import settings
from django.contrib.gis.db import models as gis_models





class Country(models.Model):
    code =  models.CharField(max_length=3)
    codeISO3 =  models.CharField(max_length=3, null=True, blank=True)
    name =  models.CharField(max_length=255, unique=True)
    currency_area = models.ForeignKey(Currency, null=True, blank=True)
    status = models.BooleanField('Active or not ?',help_text='Check if this country is available in the system', default=True)


    class Meta:
        app_label = 'decoupage'
        verbose_name = _('Pays')
        verbose_name_plural = _('Pays')

   

    def __unicode__(self):
        return u"%s"%(self.name,)

class Decoupage(MPTTModel):
    brand_name = models.CharField(_('Brand name'), max_length=500)
    country = models.ForeignKey('Country',help_text=_(u"Pays"),related_name="country",null=True,blank=True)
    decoupage_type  = models.CharField(_('Entity type'),choices=settings.DECOUPAGE_TYPE, max_length=128, null=True, blank=True)
    status = models.BooleanField('Status',help_text='Entity status, active or not', default=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='under_',db_index=True)
   


    def __unicode__(self):
        return u"%s"%(self.brand_name,)


class ReleveZonal(gis_models.Model):
    zone = models.ForeignKey('Decoupage', limit_choices_to={'decoupage_type': 'ZONE'},blank=True, null=True)
   
    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = gis_models.MultiPolygonField(srid=4326)

    def __unicode__(self):
        return u"%s"%(self.zone,)



class ZoneList(ReleveZonal):
    class Meta:
        proxy = True
        verbose_name = _('Liste des zones')


#-17.45453   14.68841