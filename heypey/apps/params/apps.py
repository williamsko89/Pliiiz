from django.apps import AppConfig


class ParamConfig(AppConfig):
    name = "params"
    icon = '<i class="material-icons">build</i>'
    verbose_name = 'Parametrage'