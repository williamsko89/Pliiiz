# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-23 15:36
from __future__ import unicode_literals

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('team', '0001_initial'),
        ('decoupage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(blank=True, max_length=50, null=True, verbose_name='Code')),
                ('name', models.CharField(blank=True, default=b'Name', max_length=50, null=True, verbose_name='Nom')),
                ('description', models.TextField(blank=True, max_length=50, null=True, verbose_name='Description')),
                ('type_value', models.CharField(blank=True, choices=[(b'1', 'CONSTANT'), (b'2', 'PERCENTAGE')], max_length=128, null=True, verbose_name='Type valeur')),
                ('value', models.DecimalField(blank=True, decimal_places=4, default=Decimal('0.00'), max_digits=20, null=True, verbose_name='Valeur')),
                ('account_type', models.CharField(blank=True, choices=[(b'1', 'PROVIDER'), (b'2', 'TEAM')], max_length=128, null=True, verbose_name="Type d'entite")),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='account_code_parent', to='params.AccountCode')),
            ],
            options={
                'verbose_name': 'Plan comptable',
                'verbose_name_plural': 'Plan comptable',
            },
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_value', models.CharField(blank=True, choices=[(b'1', 'CONSTANT'), (b'2', 'PERCENTAGE')], default=b'PERCENTAGE', max_length=128, null=True, verbose_name='Type valeur')),
                ('value', models.DecimalField(blank=True, decimal_places=2, default=0.0, max_digits=16, verbose_name='Value HT')),
                ('value_ttc', models.DecimalField(blank=True, decimal_places=2, default=0.0, max_digits=16, verbose_name='Value TTC')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Date creation')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name=b'Date modification at ')),
                ('from_zone', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='price_from_zone', to='decoupage.Decoupage')),
            ],
            options={
                'verbose_name': 'Palier de prix',
                'verbose_name_plural': 'Palier de prix',
            },
        ),
        migrations.CreateModel(
            name='PriceList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_price', models.CharField(blank=True, choices=[(b'1', 'GRILLE TARIFAIRE'), (b'2', 'GRILLE DE REMISE')], max_length=128, null=True, verbose_name='Type Grille')),
                ('name', models.CharField(blank=True, max_length=128, null=True, verbose_name='Name')),
                ('is_default', models.BooleanField(default=False, verbose_name=b'Grille par defaut ?')),
                ('team', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='team.Team', verbose_name='Team')),
            ],
            options={
                'verbose_name': 'Grille tarifaire ou remise',
                'verbose_name_plural': 'Grille tarifaire ou remise',
            },
        ),
        migrations.CreateModel(
            name='Repartition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'Name', max_length=128, null=True, verbose_name='Libelle')),
                ('entity_type', models.CharField(blank=True, choices=[(b'1', 'PROVIDER'), (b'2', 'TEAM')], max_length=128, null=True, verbose_name='Type compte')),
                ('is_standard', models.BooleanField(default=True, verbose_name=b'Standard ?')),
                ('team', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='team.Team')),
            ],
            options={
                'verbose_name': 'Repartition',
                'verbose_name_plural': 'Repartition',
            },
        ),
        migrations.CreateModel(
            name='RevenuSharingConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_type', models.CharField(blank=True, choices=[(b'1', 'PROVIDER'), (b'2', 'TEAM')], max_length=128, null=True, verbose_name='Type compte')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=b'modified at ')),
                ('sens', models.CharField(blank=True, choices=[(b'1', 'DEBIT'), (b'2', 'CREDIT')], default=b'1', max_length=128, null=True, verbose_name='Debit ou credit')),
                ('code', models.ForeignKey(default=1, help_text='Account Code', on_delete=django.db.models.deletion.CASCADE, related_name='account_code', to='params.AccountCode')),
                ('repartition', models.ForeignKey(help_text='Repartition', on_delete=django.db.models.deletion.CASCADE, related_name='repartition_', to='params.Repartition')),
            ],
            options={
                'verbose_name': 'Configuration partage commission',
                'verbose_name_plural': 'Configuration partage commission',
            },
        ),
        migrations.AddField(
            model_name='price',
            name='price_list',
            field=models.ForeignKey(help_text='Grille tarifaire ou remise', on_delete=django.db.models.deletion.CASCADE, to='params.PriceList'),
        ),
        migrations.AddField(
            model_name='price',
            name='to_zone',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='price_to_zone', to='decoupage.Decoupage'),
        ),
    ]
