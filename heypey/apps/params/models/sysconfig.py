#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2016 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"



from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext_lazy as _
from team.models import Team
from decoupage.models import Country
from django.conf import settings
from decimal import Decimal
from datetime import datetime
from django_countries.fields import CountryField



ACCOUNT_TYPE = [('1', _('PROVIDER')), ('2', _("TEAM"))]
VALUE_TYPE = [('1', _('CONSTANT')), ('2', _("PERCENTAGE"))]
PRICE_TYPE = [('1', _('GRILLE TARIFAIRE')), ('2', _("GRILLE DE REMISE"))]



class Tarif(models.Model):
    
    name  = models.CharField(_('Name'),max_length=128, null=True, blank=True)
    country = models.ForeignKey(Country,help_text=_(u"Pays"),related_name="tarif_country",null=True,blank=True)
    type_price = models.CharField(_('Type Grille'),choices=PRICE_TYPE, max_length=128, null=True, blank=True)
    is_default = models.BooleanField('Grille par defaut ?', default=False)
    
    class Meta:
        app_label = 'params'
        verbose_name = _('Tarif zone')
        verbose_name_plural = _('Tarif zone')


    def __unicode__(self):
        return u"%s"%(self.name,)

