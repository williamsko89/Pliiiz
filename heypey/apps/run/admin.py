# vim: set fileencoding=utf-8 :
from django.contrib import admin
from django.contrib.gis.db import models as gismodels
from django.contrib.gis import admin as gis_admin

from django.conf import settings
from olwidget.admin import GeoModelAdmin
from . import models


class RunAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'pliii_id',
        'pliii_demand',
        'customer',
        'customer_point',
        'team',
        'team_actor',
        'shop',
        'status_start',
        'status_waiting_pliiizer',
        'status_waiting_customer',
        'status_pending',
        'status_arrived',
        'status_end',
        'starts_at',
        'ends_at',
        'ranking',
    )
    list_filter = (
        'customer',
        'team',
        'team_actor',
        'shop',
        'status_start',
        'status_waiting_pliiizer',
        'status_waiting_customer',
        'status_pending',
        'status_arrived',
        'status_end',
        'starts_at',
        'ends_at',
    )

    def pliii_demand(self,obj):
        if obj.demand.startswith('http'):
            #return '<audio controls="controls"><source src="%s" type="audio/aac" autostart="false" ></source></audio>' % obj.demand
            return obj.demand
        else :
            return obj.demand
    pliii_demand.allow_tags = True
    pliii_demand.short_description = 'Demand'


class DemandAdmin(admin.ModelAdmin):

    list_display = (u'id', 'run', 'product', 'price','created_at',"modified_at")
    list_filter = ('run',)


class TeamActorLocationHistoryOnRunAdmin(gis_admin.OSMGeoAdmin):

    default_lat = 1654650.17223
    default_lon = -1944593.09652
    default_zoom = 13
    map_width = 1000
    map_height = 400

    list_display = (
        u'id',
        'team_actor',
        'run',
        'location',
        'created_at',
        'modified_at',
    )
    list_filter = ('team_actor', 'run', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


class RevenuAccountigAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'revenu_accounting_id',
        'Team',
        'amount',
        'status',
        'created_at',
        'modified_at',
    )
    list_filter = ('Team', 'status', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


class TeamAccountAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'team',
        'run',
        'balance_before_run',
        'amount',
        'balance_after_run',
        'created_at',
        'modified_at',
    )
    list_filter = ('team', 'run', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


class RunPathAdmin(admin.ModelAdmin):

    list_display = (u'id', 'run_point_point', 'run', 'created_at')
    list_filter = ('run', 'created_at')
    date_hierarchy = 'created_at'


class RunConversationAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'msg_from_content_type',
        'msg_from_object_id',
        'msg_to_content_type',
        'msg_to_object_id',
        'msg_type',
        'run',
        'msg_content',
        'created_at',
        'modified_at',
    )
    list_filter = ('run', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Run, RunAdmin)
_register(models.Demand, DemandAdmin)
_register(
    models.TeamActorLocationHistoryOnRun,
    TeamActorLocationHistoryOnRunAdmin)
_register(models.RevenuAccountig, RevenuAccountigAdmin)
_register(models.TeamAccount, TeamAccountAdmin)
_register(models.RunPath, RunPathAdmin)
_register(models.RunConversation, RunConversationAdmin)
