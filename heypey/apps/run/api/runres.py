#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2017 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"


from tastypie import fields
from tastypie import resources
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.conf import settings
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.serializers import Serializer
from tastypie.throttle import BaseThrottle
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.models import ContentType

from django.http import HttpResponse
from tastypie.paginator import Paginator
from exceptions import KeyError
from django.utils.translation import ugettext_lazy as _
from users.models import Customer, AppUser
from team.api import TeamResource, TeamActorResource
from shop.api import ShopResource
from users.api import CustomerResource
from django.contrib.gis.geos import Point
from run.models import Run, Demand, TeamActorLocationHistoryOnRun, RunConversation
from decoupage.models import ReleveZonal
from team.models import Team, TeamActor, TeamActorLocationHistory
from users.models import CustomerDevice
from utils import constants
from utils.ref import code
import json, simplejson
import urlparse, requests
import logging
from datetime import datetime, timedelta, date
logger = logging.getLogger(__name__)

class HeypeyResource(resources.ModelResource):
    def create_response(self, request, data, response_class=HttpResponse, **response_kwargs):
        """
        Extracts the common "which-format/serialize/return-response" cycle.

        Mostly a useful shortcut/hook.
        """
        desired_format = self.determine_format(request)
        serialized = self.serialize(request, data, desired_format)
        return response_class(content=serialized, content_type=build_content_type(desired_format), **response_kwargs)

class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content):
        pass


def build_content_type(format, encoding='utf-8'):
    """
    Appends character encoding to the provided format if not already present.
    """
    if 'charset' in format:
        return format

    return "%s; charset=%s" % (format, encoding)

class RunResource(HeypeyResource):
    customer = fields.OneToOneField(CustomerResource, 'customer',full=True,blank=True, null=True)
    team = fields.OneToOneField(TeamResource, 'team',full=True,blank=True, null=True)
    team_actor = fields.OneToOneField(TeamActorResource, 'team_actor',full=True,blank=True, null=True)
    shop = fields.OneToOneField(ShopResource, 'shop',full=True,blank=True, null=True)

    class Meta:
        queryset = Run.objects.all().order_by('starts_at')
        resource_name = 'list_run'
        name = 'list_run'
        allowed_methods = ['get', 'post']
        serializer = urlencodeSerializer() # IMPORTANT
        paginator_class = Paginator

        filtering = {
            'status': ALL,
            'customer': ALL_WITH_RELATIONS,
            'pliii_id': ALL,
        }


    def prepend_urls (self):
        return [
                url(r"^rate/$", self.wrap_view('rate'), name="rate"),
                url(r"^new_run/$", self.wrap_view('new_run'), name="new_run"),
                url(r"^accept_run_pliiizer/$", self.wrap_view('accept_run_pliiizer'), name="accept_run_pliiizer"),
                url(r"^run_invoice/$", self.wrap_view('run_invoice'), name="run_invoice"),
                url(r"^set_pliiizer_current_location/$", self.wrap_view('set_pliiizer_current_location'), name="set_pliiizer_current_location"),
                url(r"^get_pliiizer_current_location/$", self.wrap_view('get_pliiizer_current_location'), name="get_pliiizer_current_location"),
                url(r"^confirm_run_customer/$", self.wrap_view('confirm_run_customer'), name="confirm_run_customer"),
                url(r"^pliiizer_arrived/$", self.wrap_view('pliiizer_arrived'), name="pliiizer_arrived"),
                url(r"^finish_run/$", self.wrap_view('finish_run'), name="finish_run"),
                url(r"^register_chat_msg/$", self.wrap_view('register_chat_msg'), name="register_chat_msg"),
                url(r"^change_pliiizer_status/$", self.wrap_view('change_pliiizer_status'), name="change_pliiizer_status"),
               

                ]
    def determine_format(self, request):
        return 'application/json'


    def change_pliiizer_status(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliiizer_id = request.GET['pliiizer_id']
            is_available = request.GET['is_available']

            try:
                pliiizer = TeamActor.objects.get(team_actor_code=pliiizer_id)
                if is_available == '1':
                    pliiizer.is_available = False
                else:
                    pliiizer.is_available = True
                pliiizer.save()
                print 'OOOOOOK'
            except TeamActor.DoesNotExist:
                return self.create_response(request, {
                'success':False,
                'msg' : constants.UNFOUND_RUN,

            })

        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,

            })

        

    def rate(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliii_id = request.GET['pliii_id']
            rate = request.GET['rate']

            #Recherche si le customer existe dja
            try:
                run = Run.objects.get(pliii_id = pliii_id)
                run.ranking = rate
                run.save()
                
            except Run.DoesNotExist : 
               return self.create_response(request, {
                'success':False,
                'msg' : constants.UNFOUND_RUN,

            })
            
        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,

            })



    

    def accept_run_pliiizer(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            print 'accept_run_pliiizer'
            pliii_id = request.GET['pliii_id']

            print pliii_id
            accept = request.GET['accept']

            #Get pliii info
            pliii = Run.objects.get(pliii_id = pliii_id)

            #Update pliiizer status to Busy
            pliii.team_actor.status = False
            pliii.team_actor.save()

            pre_message = "Bonjour c'est %s , j'ai bien recu votre requete.Patientez , je vous renvoie la facture pour confirmation.Merci"%(pliii.team_actor)
            print pre_message

            #TODO : SEND PLIII INFO TO PLIIIZER BY MQTT NOTIFICATION
            #channel = pliiizer.team_actor_code
            channel = pliii.customer.customer_id
            content = pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_ACCEPT_PLIIIZER,pre_message)

            print channel
            print content

            
            notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
            pliiizer_notification_req = requests.get(notification_url)
           
            return self.create_response(request, {
                'success':True,
                'msg_type':constants.PLIII_STATUS_ACCEPT_PLIIIZER,
                'pliii' : pliii.detail(),
                'pre_msg' : pre_message,

            })

        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'code' : constants.KEY_ERROR,
                'msg' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'code' : constants.KEY_ERROR,
                'msg' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'code' : constants.PROCESS_OK,
                'msg' : constants.KEY_ERROR,

            })


    def run_invoice(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            body = simplejson.loads(request.body)
            invoice = body['invoice']
            pliii_id = body['pliii_id']

            print invoice
            print pliii_id
            total = 0

            pliii = Run.objects.get(pliii_id = pliii_id)

            for i in invoice:
                product = i['product']
                price = i['price']
                total+=float(price)
                #Create product
                demand = Demand()
                demand.product = product
                demand.price = price
                demand.run = pliii

                #Update status_waiting_customer  = True
                demand.run.status_waiting_customer = True
                demand.run.save()

                demand.save()

            #TODO : Get fee for pliii

            #TODO : SEND  NOTIFICATION
            #channel = pliiizer.team_actor_code
            channel = pliii.customer.customer_id

            from_name = pliii.team_actor.pliiizer_name
            to_name = pliii.customer.custormer_name

            from_id = pliii.team_actor.team_actor_code
            to_id = pliii.customer.customer_id

            pliiizer_lon = pliii.team_actor.pliiizer_lon
            pliiizer_lat =  pliii.team_actor.pliiizer_lat
            pliiizer_avatar = pliii.team_actor.pliiizer_avatar

            content = """{
                "msg_type":"%s",
                "pliii_id" : "%s",
                "invoice" : "%s",
                "pliii_amount" : "%s",
                "fee" : 0,
                "total_amount" : "%s",
                "from_name" : "%s",
                "to_name" : "%s",
                "from_id" : "%s",
                "to_id" : "%s",
                "pliiizer_lon" : "%s",
                "pliiizer_lat" : "%s",
                "pliiizer_avatar" : "%s"
                }
                """%(constants.PLIII_STATUS_INVOICE_SENT,pliii_id,invoice,total,total,from_name,to_name,from_id,to_id,pliiizer_lon,pliiizer_lat,pliiizer_avatar)
            
            
            notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)

            pliiizer_notification_req = requests.get(notification_url)
            

            #Send result to customer
            return self.create_response(request, {
                'msg_type':constants.PLIII_STATUS_INVOICE_SENT,
                'success':True,
                'pliii_id' : pliii_id,
                'invoice' : invoice,
                'pliii_amount' : total,
                'fee' : 0,
                'total_amount' : total,

            })
            
        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.KEY_ERROR,

            })


    def confirm_run_customer(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliii_id = request.GET['pliii_id']
            confirm = request.GET['confirm']


            #Get pliii info
            pliii = Run.objects.get(pliii_id = pliii_id)
            pliii.status_pending = True
            pliii.save()

            #TODO : SEND PLIII INFO TO PLIIIZER BY MQTT NOTIFICATION
            #channel = pliiizer.team_actor_code
            channel = pliii.team_actor.team_actor_code
            content = """%s"""%pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_PENDING,"")

            print channel , content
            notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
            print notification_url
            pliiizer_notification_req = requests.get(notification_url)
            print pliiizer_notification_req.status_code
            print pliiizer_notification_req.text

            return self.create_response(request, {
                'success':True,
                'msg_type':constants.PLIII_STATUS_START,
                'pliii' : pliii.detail(),

            })

            return self.create_response(request, {
                'success':True,
                'msg_type':constants.PLIII_STATUS_PENDING,
                'pliii' : pliii.detail(),

            })

        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })
         
        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })


    def set_pliiizer_current_location(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)
        try:

            lon = request.GET['lon']
            lat = request.GET['lat']
            pt = Point(float(str(lon)), float(str(lat)))
            pliiizer_id = request.GET['pliiizer_id']

        except Exception :
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        #TODO : Get team actor object once
        try:
            
            pliii_id = request.GET['pliii_id']
            print pliii_id
            
            if 'pliii_id' in request.GET:
                
                pliii = Run.objects.get(pliii_id = pliii_id,status_pending = True,pliiizer_arrived_notification_sent = False)
                
                pliiizer_location = TeamActorLocationHistoryOnRun()
                pliiizer_location.location = pt
                pliiizer_location.run = pliii
                pliiizer_location.team_actor = TeamActor.objects.get(team_actor_code = pliiizer_id)
                pliiizer_location.save()
                
                #get distance
                distance = pliii.distance
                print distance
           
                if distance <=100 :
                    #Send mqtt message to customer to show arrival popup
                    channel = pliii.customer.customer_id
                    content = """%s"""%pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_ARRIVED,"")

                    print content

                    notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
                    pliiizer_notification_req = requests.get(notification_url)
                    pliii.pliiizer_arrived_notification_sent = True
                    pliii.save()
            
            else:
                pliiizer_location = TeamActorLocationHistory()
                pliiizer_location.location = pt
                pliiizer_location.team_actor = TeamActor.objects.get(team_actor_code = pliiizer_id)
                pliiizer_location.save()
                
                
            
        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
            })
        
        except Run.DoesNotExist:
            print pt
            pliiizer_location = TeamActorLocationHistory()
            pliiizer_location.location = pt
            pliiizer_location.team_actor = TeamActor.objects.get(team_actor_code = pliiizer_id)
            pliiizer_location.save()

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })


    def get_pliiizer_current_location(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            run_id = request.GET['run_id']
            rate = request.GET['rate']

            
        except KeyError as err:
            
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,

            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })


    def pliiizer_arrived(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliii_id = request.GET['pliii_id']

            #get pliii object
            pliii = Run.objects.get(pliii_id = pliii_id)
            pliii.status_arrived = True
            pliii.save()

            #TODO : Send notif to customer to shop up the end run page

            #TODO : SEND PLIII INFO TO PLIIIZER BY MQTT NOTIFICATION
            pre_message = "JE SUIS LA"

            #TODO : SEND PLIII INFO TO PLIIIZER BY MQTT NOTIFICATION
            #channel = pliiizer.team_actor_code
            channel = pliii.customer.customer_id
            content = pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_ARRIVED,pre_message)


            print channel , content
            notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
            print notification_url
            pliiizer_notification_req = requests.get(notification_url)
            print pliiizer_notification_req.status_code
            print pliiizer_notification_req.text


            return self.create_response(request, {
                'msg_type':constants.PLIII_STATUS_ARRIVED,
                'success':True,
                'pliii' : pliii.detail(),
               

            })
            
        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })

    def finish_run(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliii_id = request.GET['pliii_id']
            rate = request.GET['rate']

            #get pliii object
            pliii = Run.objects.get(pliii_id = pliii_id)
            pliii.status_end = True
            pliii.ranking = rate
            pliii.save()

            channel = pliii.team_actor.team_actor_code
            content = pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_END,"")


            print channel , content
            notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
            print notification_url
            pliiizer_notification_req = requests.get(notification_url)
            print pliiizer_notification_req.status_code
            print pliiizer_notification_req.text

            #La course est finie. On fait le revenu sharing

            return self.create_response(request, {
                'msg_type':constants.PLIII_STATUS_END,
                'success':True,
                'pliii' : pliii.detail(),
               

            })
            
        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })

    def register_chat_msg(self, request, **kwargs):

        self.method_check(request, allowed=['post','get'])
        self.throttle = BaseThrottle(throttle_at=1, timeframe=10)


        try:
            pliii_id = request.GET['pliii_id']
            msg_type = request.GET['msg_type']
            from_user = request.GET['from_user']
            to_user = request.GET['to_user']
            message = request.GET['message']
            is_pliiizer = request.GET['is_pliiizer']


            #get pliii object
            pliii = Run.objects.get(pliii_id = pliii_id)
         
            conversatiion = RunConversation()
            if is_pliiizer == '1':
                conversatiion.msg_from_content_type =  ContentType.objects.get_for_model(TeamActor)
                conversatiion.msg_from_object_id = TeamActor.objects.get(team_actor_code = from_user).id

                conversatiion.msg_to_content_type =  ContentType.objects.get_for_model(Customer)
                conversatiion.msg_to_object_id = Customer.objects.get(customer_id = to_user).id
            else:
                conversatiion.msg_from_content_type =  ContentType.objects.get_for_model(Customer)
                conversatiion.msg_from_object_id =Customer.objects.get(customer_id = from_user).id

                conversatiion.msg_to_content_type =  ContentType.objects.get_for_model(TeamActor)
                conversatiion.msg_to_object_id = TeamActor.objects.get(team_actor_code = to_user).id

            conversatiion.msg_type = msg_type
            conversatiion.run = pliii
            conversatiion.msg_content = message

            conversatiion.save()
            #TOFO :  Enregistrer la date


        except KeyError as err:
            print err
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })

        except Exception as e:
            print e
            return self.create_response(request, {
                'success':False,
                'msg' : constants.KEY_ERROR,
                'code' : constants.KEY_ERROR,
            })


        return self.create_response(request, {
                'success':True,
                'msg' : constants.PROCESS_OK,
                'code' : constants.PROCESS_OK,

            })



class PliiizerLocationHistoryResource(HeypeyResource):
    run = fields.OneToOneField(RunResource, 'run',full=True,blank=True, null=True)
    team_actor = fields.OneToOneField(TeamActorResource, 'team_actor',full=True,blank=True, null=True)

    class Meta:
        queryset = TeamActorLocationHistoryOnRun.objects.all()
        resource_name = 'get_pliiizer_current_locations'
        name = 'get_pliiizer_current_locations'
        allowed_methods = ['get', 'post']
        serializer = urlencodeSerializer() # IMPORTANT

        filtering = {
            'status': ALL,
            'team_actor': ALL_WITH_RELATIONS,
            'run': ALL_WITH_RELATIONS,
        }



