from django import forms

class UploadFileForm(forms.Form):
    request = forms.CharField(max_length=50)
    uuid = forms.CharField(max_length=50)
    lon = forms.CharField(max_length=50)
    lat = forms.CharField(max_length=50)
    audio = forms.FileField()