"""Base models"""
#!/usr/bin/env python
#-*- coding: utf-8 -*-

__author__ = "William de SOUZA"

from django.db import models
from django.contrib.gis.db import models as gis_models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import fields
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from utils.rand import rand
from utils.ref import ref
from decimal import Decimal
from utils import constants
from users.models import Customer
from team.models import TeamActor, Team
from params.models import Tarif
from shop.models import Shop
from django.contrib.gis.measure import Distance, D
from geopy.distance import distance


    
class Run(models.Model):
    pliii_id = models.CharField(max_length=18, default=ref, unique=True)
    demand = models.TextField(_('Product list'),max_length=5000, )
    customer = models.ForeignKey(Customer,null = True, )
    customer_point = gis_models.PointField(srid=4326,null=True, blank=True)
    team = models.ForeignKey(Team)
    team_actor = models.ForeignKey(TeamActor)
    fee = models.ForeignKey(Tarif,null = True,blank=True)
    shop = models.ForeignKey(Shop,null = True, blank = True)
    status_start = models.BooleanField('Status Start ', default=False)
    status_waiting_pliiizer = models.BooleanField('status_waiting_pliiizer', default=False)
    status_waiting_customer = models.BooleanField('status_waiting_customer', default=False)
    status_pending = models.BooleanField('Status Pending ', default=False)
    status_arrived = models.BooleanField('Status arrvied ', default=False)
    status_end = models.BooleanField('Status end ', default=False)
    pliiizer_arrived_notification_sent = models.BooleanField('Pliiizer arrived notification sent ', default=False)
    starts_at = models.DateTimeField(_('Date debut'),auto_now_add=True,null=True, blank=True,)
    ends_at = models.DateTimeField(_('Date fin'),auto_now=True,null=True, blank=True,)
    ranking = models.IntegerField(_('Ranking'),max_length=18,default=0)
    is_audio = models.BooleanField('Is Audio Request', default=False)


    class Meta:
        app_label = 'run'
        verbose_name = _('Pliii')
        verbose_name_plural = _('Pliiis')

    def __unicode__(self):
        return "%s" % ((self.pliii_id))

    def detail(self):
        return {
        'pliii_id':self.pliii_id,
        'request':self.demand,
        'pliiizer' : self.team_actor.detail(),

        }

    def detail_mqtt(self,msg_type):
        team_actor_location = TeamActorLocationHistoryOnRun.objects.filter(team_actor = self.team_actor).last().location
        return """{
        "msg_type": "%s",
        "pliii_id":"%s",
        "request":"%s",
        "pliiizer_id":"%s",
        "pliiizer_name":"%s",
        "pliiizer_lon":"%s",
        "pliiizer_lat":"%s",
        "pliiizer_avatar":"%s",
        "customer_id":"%s",
        "customer_name":"%s",
        "customer_avatar":"%s",
        "is_audio":"%s",
        "audio":"%s",
        "rating":"%s"
        }"""%(msg_type,self.pliii_id,self.demand,self.team_actor.team_actor_code,\
            self.team_actor.user.first_name+" "+self.team_actor.user.last_name,team_actor_location.x,\
            team_actor_location.y,self.team_actor.pliiizer_avatar,\
            self.customer.customer_id,self.customer.custormer_name,self.customer.customer_avatar,self.is_audio,self.demand,self.ranking)

    
    
    def detail_mqtt_accept_run_pliiizer(self,msg_type,pre_msg):

        team_actor_location = TeamActorLocationHistoryOnRun.objects.filter(team_actor = self.team_actor).last().location
        return """{
        "msg_type": "%s",
        "pliii_id":"%s",
        "request":"%s",
        "pre_msg" : "%s",
        "pliiizer_id":"%s",
        "pliiizer_name":"%s",
        "pliiizer_lon":"%s",
        "pliiizer_lat":"%s",
        "pliiizer_avatar":"%s",
        "customer_id":"%s",
        "customer_name":"%s",
        "customer_avatar":"%s",
        "is_audio":"%s",
        "audio":"%s",
        "customer_lon":"%s",
        "customer_lat":"%s",
        "rating":"%s"
        }"""%(msg_type,self.pliii_id,self.demand,pre_msg,self.team_actor.team_actor_code,\
            self.team_actor.user.first_name+" "+self.team_actor.user.last_name,team_actor_location.x,\
            team_actor_location.y,self.team_actor.pliiizer_avatar,\
            self.customer.customer_id,self.customer.custormer_name,self.customer.customer_avatar,self.is_audio,self.demand,\
            self.customer_point.x,self.customer_point.y,self.ranking)


    @property
    def distance(self):
        team_actor_location = TeamActorLocationHistoryOnRun.objects.filter(team_actor = self.team_actor).last().location
        print team_actor_location
        return team_actor_location.distance(self.customer_point)*100000


class Demand(models.Model):
    run = models.ForeignKey(Run,related_name = 'run_demand')
    product = models.CharField(max_length=100)
    price = models.DecimalField(_(u"Prix produit"),max_digits=20,decimal_places=2,default=Decimal('0.00'),null=True,blank=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)
   

    class Meta:
        app_label = 'run'
        verbose_name = _('Liste produits')
        verbose_name_plural = _('Liste produits')

    def __unicode__(self):
        return "%s" % ((self.product))

    def detail(self):
        return {
        'product':self.product,
        'price':self.price,
        }


class TeamActorLocationHistoryOnRun(gis_models.Model):
    team_actor = gis_models.ForeignKey(TeamActor)
    run = gis_models.ForeignKey("Run")
    location = gis_models.PointField(srid=4326)
    created_at = gis_models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= gis_models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'run'
        verbose_name = _('Localication Pliiizer')
        verbose_name_plural = _('Localication Pliiizer')

    def __unicode__(self):
        return "%s" % ((self.team_actor))

class RevenuAccountig(models.Model):
    revenu_accounting_id = models.CharField(max_length=18, default=rand, unique=True)
    Team = models.ForeignKey(Team)
    amount = models.DecimalField(_(u"Montant"),max_digits=20,decimal_places=4,default=Decimal('0.00'),null=True,blank=True)
    status = models.BooleanField('Status', default=True)
    created_at = models.DateTimeField(_('Date ecriture'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'run'
        verbose_name = _('Partage commission')
        verbose_name_plural = _('Partage commission')

    def __unicode__(self):
        return "%s " % ((self.revenu_accounting_id))



class TeamAccount(models.Model):

    team = models.ForeignKey(Team, null=True, blank=True)
    run = models.ForeignKey('Run', null=True, blank=True)
    balance_before_run = models.DecimalField(_(u" Solde avant la Course"),max_digits=20,decimal_places=4,default=Decimal('0.00'))
    amount = models.DecimalField(_(u" Montant"),max_digits=20,decimal_places=4,default=Decimal('0.00'))
    balance_after_run = models.DecimalField(_(u" Solde apres la Course"),max_digits=20,decimal_places=4,default=Decimal('0.00'))
    created_at = models.DateTimeField(_('Date ecriture'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)    

    class Meta:
        app_label = 'run'
        verbose_name = _('Compte Team')
        verbose_name_plural = _('Compte Team')


    def __unicode__(self):
        return u"%s %s"%(self.team ,self.balance)



class RunPath(gis_models.Model):
    run_point_point = gis_models.PointField(srid=4326,null=True, blank=True)
    run = models.ForeignKey('Run', null=True, blank=True)
    created_at = models.DateTimeField(_('Date position'),auto_now_add=True,null=True, blank=True,)


    class Meta:
        app_label = 'run'
        verbose_name = _('Trajet Course')
        verbose_name_plural = _('Trajet Course')

    def __unicode__(self):
        return "%s" % ((self.run))


class RunConversation(models.Model):
    msg_id = models.CharField(max_length=18, default=ref, unique=True)

    msg_from_limit = models.Q(app_label='users', model='customer') | models.Q(app_label='team', model='teamactor')
    msg_from_content_type = models.ForeignKey(ContentType,verbose_name=_('client ou Pliiizer'),limit_choices_to=msg_from_limit,null=True,blank=True,related_name='msg_from')
    msg_from_object_id = models.PositiveIntegerField(verbose_name=_(' Objet'),null=True,)
    msg_from_content_object = fields.GenericForeignKey('msg_from_content_type', 'msg_from_object_id')


    msg_to_limit = models.Q(app_label='users', model='customer') | models.Q(app_label='team', model='teamactor')
    msg_to_content_type = models.ForeignKey(ContentType,verbose_name=_('client ou Pliiizer'),limit_choices_to=msg_to_limit,null=True,blank=True,related_name='msg_to')
    msg_to_object_id = models.PositiveIntegerField(verbose_name=_(' Objet'),null=True,)
    msg_to_content_object = fields.GenericForeignKey('msg_to_content_type', 'msg_to_object_id')

    msg_type  = models.CharField(_('Type message'),choices=constants.MESSAGE_TYPE, max_length=128, null=True, blank=True,default='TEXT')

    run = models.ForeignKey('Run', null=True, blank=True)
    msg_content = models.CharField(max_length=1000)
    created_at = models.DateTimeField(_('Date emission'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date  reception ', auto_now=True)    

    class Meta:
        app_label = 'run'
        verbose_name = _('Conversation')
        verbose_name_plural = _('Conversations')


    def __unicode__(self):
        return u"%s %s"%(self.team ,self.balance)

    
