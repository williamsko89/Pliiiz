"""urlconf for the base application"""


from django.conf.urls import url

from .views import new_run


urlpatterns = [
    url(r'^new_run/?$', new_run,name='new_run'),
  
]
