#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2017 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"


from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from exceptions import KeyError
from django.utils.translation import ugettext_lazy as _
from users.models import Customer, AppUser
from django.contrib.gis.geos import Point
from run.models import Run, Demand, TeamActorLocationHistoryOnRun, RunConversation
from decoupage.models import ReleveZonal
from team.models import Team, TeamActor
from users.models import CustomerDevice
from utils import constants
from utils.ref import code
import json, simplejson
import urlparse, requests
import logging
from datetime import datetime, timedelta, date
logger = logging.getLogger(__name__)
from django.core.files.storage import FileSystemStorage


from django.http import HttpResponseRedirect
from django.shortcuts import render
from run.forms import UploadFileForm

# Imaginary function to handle an uploaded file.


def process_run(request,file_name):

        try:
            print "HELLOOOOOOOO2"
            audio = file_name
            print "HELLOOOOOOOO3"

            
            body = request.POST

            print body
           
            req = body['request']
            uuid = body['uuid']
            lon = body['lon']
            lat = body['lat']

            #TODO : get customer zone
            pt = Point(float(lon), float(lat))
            print pt

            #We must have only one zone
            try:
                zone = ReleveZonal.objects.filter(mpoly__intersects=pt).first()
                if zone is None:
                    result =  {
                        'success':False,
                        'msg' : constants.UNCOVERED_ZONE,
                    }
                    data = simplejson.dumps(result)
                    #Do not comment
                    #return data

            except ReleveZonal.DoesNotExist as err:
                print err
                result = {
                    'success':False,
                    'msg' : constants.UNCOVERED_ZONE,
                }
                data = simplejson.dumps(result)
                #Do not comment
                #return data


            
            try:
                #retreive the team of the zone
                #team = Team.objects.get(zone  = zone)

                #use this for test
                team = Team.objects.get(zone__id  = 1)
                #get one actor
                
                #pliiizer = TeamActor.objects.filter(status=True,team = team).order_by('?').first()

                #pliiizer = TeamActor.objects.filter(team = team).order_by('?').first()
                pliiizer = TeamActor.objects.get(team_actor_code = '589526')
                print pliiizer

                #Record the pliii
                pliii = Run()
                if file_name is not None : 
                    pliii.demand = file_name
                else:
                    pliii.demand = req
                pliii.customer = CustomerDevice.objects.get(device_id = uuid).customer
                pliii.customer_point = pt
                pliii.team_actor = pliiizer
                pliii.team = team
                pliii.status_start = True
                pliii.status_waiting_pliiizer = True

                if 'audio' in request.FILES:
                    pliii.is_audio = True
                pliii.save()

                #Set pliiizer location


                pliiizer_location = TeamActorLocationHistoryOnRun()
                pliiizer_pt = Point(pliiizer.pliiizer_lon,pliiizer.pliiizer_lat)
                pliiizer_location.location = pliiizer_pt
                pliiizer_location.run = pliii
                pliiizer_location.team_actor = pliiizer
                pliiizer_location.save()

                #TODO : SEND PLIII INFO TO PLIIIZER BY MQTT NOTIFICATION
                channel = pliiizer.team_actor_code
                content = """%s"""%pliii.detail_mqtt_accept_run_pliiizer(constants.PLIII_STATUS_START,"")
                print content

                notification_url = settings.MQTT_SERVER_ENDPOINT+"?channel=%s&content=%s"%(channel,content)
                pliiizer_notification_req = requests.get(notification_url)

                
                ##Simulate accept action
                
                #payload = {'pliii_id':pliii.pliii_id, 'accept': '1'}
                #url = "http://127.0.0.1:8001/api/v0/accept_run_pliiizer"
                #requests.get(url, params=payload)
               

                result = {
                    'success':True,
                    'msg_type':constants.PLIII_STATUS_START,
                    'pliii' : pliii.detail(),
                    'audio' : audio
                }
                data = simplejson.dumps(result)
                return data

            except Team.DoesNotExist as e:
                print e
                result = {
                    'success':False,
                    'msg' : constants.UNVAVAILABLE_TEAM,
                }
                data = simplejson.dumps(result)
                return HttpResponse(data, content_type='application/json')

            except TeamActor.DoesNotExist as e:
                print e
                result =  {
                'success':False,
                'msg' : constants.UNVAVAILABLE_PLIIIZER,
            }
                
                #TODO : Alert system zone uncovered
            except Exception as e:
                print e
                result =  {
                'success':False,
                'msg' : constants.KEY_ERROR,
                }
                data = simplejson.dumps(result)
                return data


        except KeyError as err:

            result = {
                'success':False,
                'msg' : constants.KEY_ERROR,
            }
            data = simplejson.dumps(result)
            return data

        except Exception as e:
            print e
            result =  {
                'success':False,
                'msg' : constants.KEY_ERROR,
            }
            data = simplejson.dumps(result)
            return data


        result =  {
                'success':True,
                'msg' : constants.PROCESS_OK,

            }
        data = simplejson.dumps(result)
        return data



def handle_uploaded_file(f):
    with open(AUDIO_UPLOAD_PATH+f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


    return AUDIO_UPLOAD_PATH+f.name


@csrf_exempt
def new_run(request):

    print request.method
    print request
    print request.POST

    if request.method == 'POST':

       
        if 'audio' in request.FILES:
            print "HOLLLLAAAAAAAAA"
            audio = request.FILES['audio']
            fs = FileSystemStorage()
            filename = fs.save(request.FILES['audio'].name, audio)
            uploaded_file_url = fs.url(filename)
            uploaded_file_url = constants.SERVER_URL+uploaded_file_url
        else:
            print "HELLOOOOOOOO1"
            uploaded_file_url = None

        result = process_run(request,uploaded_file_url)
            
        print result
         
        return HttpResponse(result, content_type='application/json')
            
    else:
        result =  {
                        'success':False,
                        'msg' : constants.UNCOVERED_ZONE,
                        'code' : constants.UNCOVERED_ZONE,
                    }
        data = simplejson.dumps(result)
    return HttpResponse(result, content_type='application/json')


