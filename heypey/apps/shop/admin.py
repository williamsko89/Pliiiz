# vim: set fileencoding=utf-8 :
from django.contrib import admin
from django.contrib.gis import admin as gis_admin
from django.forms.models import BaseInlineFormSet
from django.contrib.admin import TabularInline

from . import models


class ShopItemPictureFormSet(BaseInlineFormSet):
    def clean(self):
        super(RevenuSharingFormSet, self).clean()


class ShopItemPictureInline(TabularInline):
    model = models.ShopItemPicture
    extra = 0
    formset = ShopItemPictureFormSet

   

    def get_extra (self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra

    def get_formset(self, request, obj=None, **kwargs):

        formset = super(ShopItemPictureInline, self).get_formset(request, obj, **kwargs)
        return formset


class ShopAdmin(gis_admin.OSMGeoAdmin):
    inlines = [ShopItemPictureInline]


    default_lat = 1654650.17223
    default_lon = -1944593.09652
    default_zoom = 13
    map_width = 1000
    map_height = 400

    
    list_display = (
        u'id',
        'shop_id',
        'manager',
        'city',
        'country',
        'phone',
        'street',
        'created_at',
        'modified_at',
        'logo',
        'zone',
        'location',
    )
    list_filter = ('manager', 'created_at', 'modified_at', 'zone')
    date_hierarchy = 'created_at'


class ShopItemPictureAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'avatar_id',
        'shop',
        'picture_legend',
        'picture_path',
        'status',
        'created_at',
        'modified_at',
    )
    list_filter = ('shop', 'status', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Shop, ShopAdmin)
_register(models.ShopItemPicture, ShopItemPictureAdmin)
