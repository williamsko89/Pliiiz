# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-08-28 11:13
from __future__ import unicode_literals

from django.db import migrations, models
import sorl.thumbnail.fields
import utils.rand


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20170828_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shopitempicture',
            name='avatar_id',
            field=models.CharField(default=utils.rand.rand, max_length=18, unique=True, verbose_name='ID Menu'),
        ),
        migrations.AlterField(
            model_name='shopitempicture',
            name='picture_legend',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Note'),
        ),
        migrations.AlterField(
            model_name='shopitempicture',
            name='picture_path',
            field=sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to=b'info/images', verbose_name=b'Image menu'),
        ),
    ]
