"""Base models"""
#!/usr/bin/env python
#-*- coding: utf-8 -*-

__author__ = "William de SOUZA"

from django.db import models
from django.conf import settings
from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _
from utils.rand import rand
from sorl.thumbnail import ImageField
from decoupage.models import Decoupage
from django.contrib.gis.db import models as gis_models



    
class Shop(gis_models.Model):
    shop_id = gis_models.CharField(max_length=18, default=rand, unique=True)
    manager = gis_models.OneToOneField(settings.AUTH_USER_MODEL, editable=True, related_name='shop_manager',null=True)
    city = gis_models.CharField(_('Ville'), max_length=1000, null=True, blank=True)
    country  = CountryField()
    phone = gis_models.CharField(_('Telephone'), max_length=128, null=True, blank=True)
    street = gis_models.CharField(_('Adresse'), max_length=128, null=True, blank=True)
    created_at = gis_models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= gis_models.DateTimeField('Date modification at ', auto_now=True)
    logo = ImageField("Logo", help_text="Logo", upload_to='info/images',blank=True, null=True)
    zone = gis_models.ForeignKey(Decoupage, limit_choices_to={'decoupage_type': 'ZONE'},blank=True, null=True)
    location = gis_models.PointField(srid=4326,null=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, editable=True, related_name='pliiizer',null=True,help_text=_("Boutique ajoutee par"))





    class Meta:
        app_label = 'shop'
        verbose_name = _('Commerce')
        verbose_name_plural = _('Commerce')

    def __unicode__(self):
        return "%s" % ((self.shop_id))



class ShopItemPicture(models.Model):
    avatar_id = models.CharField(_('ID Menu'),max_length=18, default=rand, unique=True)
    shop = models.ForeignKey(Shop)
    picture_legend = models.CharField(_('Note'), max_length=128, null=True, blank=True)
    picture_path = ImageField("Image menu", upload_to='info/images',blank=True, null=True)
    status = models.BooleanField('Status', default=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'shop'
        verbose_name = _('Image menu')
        verbose_name_plural = _('Image menu')

    def __unicode__(self):
        return "%s " % ((self.picture_path))