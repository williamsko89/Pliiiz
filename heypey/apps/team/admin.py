# vim: set fileencoding=utf-8 :
from django.contrib import admin
from django.contrib.gis.db import models as gismodels
from django.contrib.gis import admin as gis_admin
from django.utils.translation import ugettext_lazy as _

from . import models


class TeamAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'brand_name',
        'team_code',
        'ninea',
        'logo',
        'status',
        'zone',
        'team_manager',
    )
    list_filter = ('status', 'zone')


    def team_manager(self,obj):
        try:
            team_actor = models.TeamActor.objects.get(is_team_mamager = True, team = obj)
            return team_actor
        except Exception as e:
            return '-'


    team_manager.allow_tags = True
    team_manager.short_description = _('Manager Zone')


class TeamActorAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'team_actor_code',
        'user',
        'team',
        'adress',
        'phone_number',
        'status',
        'type_identification',
        'identification_number',
        'deleivery_date',
        'expiry_date',
        'identification_image',
        'is_team_mamager',
        'is_available',
    )
    list_filter = (
        'user',
        'team',
        'status',
        'deleivery_date',
        'expiry_date',
        'is_team_mamager',
    )


class TeamActorLocationHistoryAdmin(gis_admin.OSMGeoAdmin):

    default_lat = 1654650.17223
    default_lon = -1944593.09652
    default_zoom = 13
    map_width = 1000
    map_height = 400
    
    list_display = (
        u'id',
        'team_actor',
        'location',
        'created_at',
        'modified_at',
    )
    list_filter = ('team_actor', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


class TeamActorProfilePictureAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'avatar_id',
        'team_actor',
        'picture_legend',
        'picture_path',
        'status',
        'created_at',
        'modified_at',
    )
    list_filter = ('team_actor', 'status', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Team, TeamAdmin)
_register(models.TeamActor, TeamActorAdmin)
_register(models.TeamActorLocationHistory, TeamActorLocationHistoryAdmin)
_register(models.TeamActorProfilePicture, TeamActorProfilePictureAdmin)
