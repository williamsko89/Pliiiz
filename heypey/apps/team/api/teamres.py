#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2016 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"

from tastypie import fields
from tastypie import resources
from django.conf import settings
from django.conf.urls import url
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.serializers import Serializer
from tastypie.throttle import BaseThrottle
from django.http import HttpResponse
from exceptions import KeyError
from django.utils.translation import ugettext_lazy as _
from team.models import Team , TeamActor
from users.api import HeypeyResource
import json
import urlparse
import logging
logger = logging.getLogger(__name__)

class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content):
        pass


def build_content_type(format, encoding='utf-8'):
    """
    Appends character encoding to the provided format if not already present.
    """
    if 'charset' in format:
        return format

    return "%s; charset=%s" % (format, encoding)


class TeamResource(HeypeyResource):
   
    class Meta:
        queryset = Team.objects.all()
        resource_name = 'team'
        name = 'team'
        allowed_methods = ['get', 'post']
        serializer = urlencodeSerializer() # IMPORTANT




class TeamActorResource(HeypeyResource):
   
    class Meta:
        queryset = TeamActor.objects.all()
        resource_name = 'team_actor'
        name = 'team_actor'
        allowed_methods = ['get', 'post']
        serializer = urlencodeSerializer() # IMPORTANT

