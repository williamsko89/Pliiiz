#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Copyright (C) 2016 Compeel

__author__ = "William de SOUZA ♟ <william.desouza@compeel.com>"



from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from django.conf import settings
from utils.ref import ref, code
from utils.rand import rand
from decoupage.models import Decoupage, ReleveZonal
from django.contrib.gis.db import models as gis_models
from utils import constants





class Team(models.Model):
    brand_name = models.CharField(_('Brand name'), max_length=500)
    team_code  = models.CharField(_('Identifiant'),max_length=128, null=True, blank=True,default=code)
    ninea = models.CharField(_('Numero registre de commerce'), max_length=128, null=True, blank=True)
    logo = ImageField("Logo", help_text="Logo Equipe", upload_to='entity/images',blank=True, null=True)
    status = models.BooleanField('Statut', default=True)
    zone = models.ForeignKey(ReleveZonal, blank=True, null=True)

    def __unicode__(self):
        return u"%s"%(self.brand_name,)
    
    class Meta:
        app_label = 'team'
        verbose_name = _('Team Pliiizer')
        verbose_name_plural = _('Team Pliiizer')


class TeamActor(models.Model):
    team_actor_code  = models.CharField(_('Identifiant'),max_length=128, null=True, blank=True,default=code)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, editable=True, related_name='team_actor',null=True)
    team= models.ForeignKey(Team)
    adress = models.CharField(_('Address'), max_length=128, null=True, blank=True)
    phone_number = models.CharField(_("Phone number"), max_length=500,blank=True,null=True,default=' ')
    status = models.BooleanField('Statut', default=True)
    type_identification = models.CharField(_('Type identification'),choices=settings.IDENTIFICATION_TYPE, max_length=128, null=True, blank=True)
    identification_number = models.CharField(_('Numero identification'),max_length=50,null=True,blank=True)
    deleivery_date= models.DateTimeField('Date de delivrance de la piece ', auto_now=True)
    expiry_date= models.DateTimeField('Date Expiration', auto_now=True)
    identification_image = ImageField("Avatar", help_text="Logo entite ", upload_to='entity/images',blank=True, null=True)
    is_team_mamager = models.BooleanField(_('Manager Equipe'), default=False)
    is_available = models.BooleanField(_('Indique si le pliiizer est disponible ou non'), default=True)
    created_at = gis_models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= gis_models.DateTimeField('Date modification at ', auto_now=True)

    def __unicode__(self):
        return u"%s"%(self.user.first_name)
    
    class Meta:
        app_label = 'team'
        verbose_name = _('Pliiizer')
        verbose_name_plural = _('Pliiizers')


    def detail(self):


        try:
            avatar = TeamActorProfilePicture.objects.filter(team_actor = self).last()
        except Exception as e:
            avatar = None

        try:
            last_location = TeamActorLocationHistory.objects.filter(team_actor = self).last().location
            if last_location is None:
                pliiizer_lon = None
                pliiizer_lat = None
            else:
                pliiizer_lon = last_location.x
                pliiizer_lat = last_location.y
        except Exception as e:
            pass

        pliiizer_lat = 14.707090
        pliiizer_lon = -17.474100

        return{
            'team' : self.team.brand_name,
            'name': self.user.first_name + " "+ self.user.last_name,
            'address' : self.adress,
            'phone_number' : self.phone_number,
            'id' : self.team_actor_code,
            'lon' : pliiizer_lon,
            'lat' : pliiizer_lat,
            'avatar' : self.pliiizer_avatar,
            'is_available' : self.is_available,
        }


    @property
    def pliiizer_lon(self):
        try:
            last_location = TeamActorLocationHistory.objects.filter(team_actor = self).last().location
            return last_location.x
        except Exception as e:
            return None

    @property
    def pliiizer_name(self):
        return u"%s %s "%(self.user.first_name , self.user.last_name)

    @property
    def pliiizer_lat(self):
        try:
            last_location = TeamActorLocationHistory.objects.filter(team_actor = self).last().location
            return last_location.y
        except Exception as e:
            return None

    @property
    def pliiizer_avatar(self):
        try:
            avatar = constants.SERVER_URL+TeamActorProfilePicture.objects.filter(team_actor = self).last().picture_path.url
            print avatar
        except Exception as e:
            print e
            avatar = None
        return avatar





class TeamActorLocationHistory(gis_models.Model):
    team_actor = gis_models.ForeignKey("TeamActor")
    location = gis_models.PointField(srid=4326)
    created_at = gis_models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= gis_models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'team'
        verbose_name = _('Position pliiizer')
        verbose_name_plural = _('Position pliiizers')

    def __unicode__(self):
        return "%s" % ((self.team_actor))


class TeamActorProfilePicture(models.Model):
    avatar_id = models.CharField(max_length=18, default=rand, unique=True)
    team_actor = models.ForeignKey(TeamActor)
    picture_legend = models.CharField(_('Legende'), max_length=128, null=True, blank=True)
    picture_path = ImageField("Logo", help_text="Logo de la boutique ", upload_to='info/images',blank=True, null=True)
    status = models.BooleanField('Status', default=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'team'
        verbose_name = _('Image de profil Pliiizer')
        verbose_name_plural = _('Image de profil Pliiizer')

        
    def __unicode__(self):
        return "%s " % ((self.picture_path))

