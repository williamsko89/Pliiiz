# vim: set fileencoding=utf-8 :
from django.contrib import admin

from . import models


class AppUserAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'password',
        'last_login',
        'is_superuser',
        'username',
        'first_name',
        'last_name',
        'email',
        'is_staff',
        'is_active',
        'date_joined',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
    )
    raw_id_fields = ('groups',)


class CustomerAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'customer_id',
        'user',
        'city',
        'country',
        'phone',
        'street',
        'street_job',
        'other_adress',
        'created_at',
        'modified_at',
        'zone',
        'is_pliiizer',
        'status',
    )
    list_filter = ('user', 'created_at', 'modified_at', 'zone', 'status')
    date_hierarchy = 'created_at'


class CustomerDeviceAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'customer',
        'device_id',
        'device_imei',
        'created_at',
        'modified_at',
        'status',
    )
    list_filter = ('customer', 'created_at', 'modified_at', 'status')
    date_hierarchy = 'created_at'


class CustomerGeneratedPassCodeAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'customer',
        'pass_code',
        'created_at',
        'modified_at',
        'status',
    )
    list_filter = ('customer', 'created_at', 'modified_at', 'status')
    date_hierarchy = 'created_at'


class CustomerLocationHistoryAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'customer',
        'location',
        'created_at',
        'modified_at',
    )
    list_filter = ('customer', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'


class CustomerProfilePictureAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'avatar_id',
        'customer',
        'picture_legend',
        'customer_image_profile',
        'status',
        'created_at',
        'modified_at',
    )
    list_filter = ('customer', 'status', 'created_at', 'modified_at')
    date_hierarchy = 'created_at'

    def customer_image_profile(self,obj):
        return '<img src="%s" height="100" width="100"/>' % obj.picture_path
    customer_image_profile.allow_tags = True
    customer_image_profile.short_description = 'Profil'


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.AppUser, AppUserAdmin)
_register(models.Customer, CustomerAdmin)
_register(models.CustomerDevice, CustomerDeviceAdmin)
_register(models.CustomerGeneratedPassCode, CustomerGeneratedPassCodeAdmin)
_register(models.CustomerLocationHistory, CustomerLocationHistoryAdmin)
_register(models.CustomerProfilePicture, CustomerProfilePictureAdmin)
