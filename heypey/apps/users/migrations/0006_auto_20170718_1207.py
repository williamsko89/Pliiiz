# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-18 12:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_customer_customer_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerprofilepicture',
            name='picture_path',
            field=models.CharField(blank=True, help_text=b'Image de profil', max_length=128, null=True, verbose_name=b'Image de profil'),
        ),
    ]
