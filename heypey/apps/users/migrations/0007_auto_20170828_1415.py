# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-08-28 14:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20170718_1207'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customergeneratedpasscode',
            options={'verbose_name': 'Code envoye', 'verbose_name_plural': 'Code envoye'},
        ),
        migrations.AlterModelOptions(
            name='customerlocationhistory',
            options={'verbose_name': 'Position Client', 'verbose_name_plural': 'Position Client'},
        ),
        migrations.AlterField(
            model_name='customer',
            name='user',
            field=models.OneToOneField(help_text='Nom client', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer', to=settings.AUTH_USER_MODEL),
        ),
    ]
