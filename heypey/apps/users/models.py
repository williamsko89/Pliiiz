"""Base models"""
#!/usr/bin/env python
#-*- coding: utf-8 -*-

__author__ = "William de SOUZA"

from django.db import models
from django.conf import settings
from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from utils.rand import rand
from sorl.thumbnail import ImageField
from decoupage.models import Decoupage
from django.contrib.gis.db import models as gis_models
from utils.ref import ref, code
from team.models import TeamActor
from utils import constants

class AppUser(AbstractUser):
    class Meta:
        app_label = 'users'
        verbose_name = _('Module utilisateur')
        verbose_name_plural = _('Module utilisateur')


    
class Customer(models.Model):
    
    user = models.OneToOneField(settings.AUTH_USER_MODEL, editable=True, related_name='customer',null=True,help_text=_("Nom client"))
    customer_id = models.CharField(max_length=18 , default=rand)
    city = models.CharField(_('Ville'), max_length=1000, null=True, blank=True)
    country  = CountryField()
    phone = models.CharField(_('Telephone'), max_length=128, null=True, blank=True)
    street = models.CharField(_('Adresse domicile'), max_length=128, null=True, blank=True)
    street_job = models.CharField(_('Adresse Job'), max_length=128, null=True, blank=True)
    other_adress = models.CharField(_('Autre Adresse'), max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)
    zone = models.ForeignKey(Decoupage, limit_choices_to={'decoupage_type': 'ZONE'},blank=True, null=True,related_name='customer_zone')
    is_pliiizer = models.BooleanField('Is this customer a pliiizer ?', default=False)
    status = models.BooleanField('Status', default=False)
    


    @property
    def custormer_name(self):
        return u"%s"%(self.user.first_name)


    def detail(self):

        try:
            avatar = constants.SERVER_URL+'/'+CustomerProfilePicture.objects.filter(customer = self).last().picture_path
        except Exception as e:
            avatar = None
        return {
        "id":self.customer_id,
        "name":self.user.first_name ,
        "email":self.user.email,
        "address":self.street,
        "phone":self.phone,
        "avatar" : avatar
        }

    @property
    def customer_avatar(self):
        try:
            avatar = constants.SERVER_URL+'/'+CustomerProfilePicture.objects.filter(customer = self).last().picture_path
        except Exception as e:
            avatar = None
        return avatar



    class Meta:
        app_label = 'users'
        verbose_name = _('Client')
        verbose_name_plural = _('Clients')

    def __unicode__(self):
        return "%s" % ((self.user))



class CustomerDevice(models.Model):
    customer = models.ForeignKey("Customer",related_name="customer_device")
    device_id = models.CharField(_('ID Telephone'), max_length=500, null=True, blank=True,unique=True)
    device_imei = models.CharField(_('IMEI Telephone'), max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)
    status = models.BooleanField('Status', default=True)


    class Meta:
        app_label = 'users'
        verbose_name = _('Client Appareil')
        verbose_name_plural = _('Clients Appareils')

    def __unicode__(self):
        return "%s" % ((self.customer))


class CustomerGeneratedPassCode(models.Model):
    customer = models.ForeignKey("Customer")
    pass_code = models.CharField(_('ID Telephone'), max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)
    status = models.BooleanField('Status', default=True)


    class Meta:
        app_label = 'users'
        verbose_name = _('Code envoye')
        verbose_name_plural = _('Code envoye')

    def __unicode__(self):
        return "%s" % ((self.customer))


class CustomerLocationHistory(gis_models.Model):
    customer = gis_models.ForeignKey("Customer")
    location = gis_models.PointField(srid=2276)
    created_at = gis_models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= gis_models.DateTimeField('Date modification at ', auto_now=True)


    class Meta:
        app_label = 'users'
        verbose_name = _('Position Client')
        verbose_name_plural = _('Position Client')

    def __unicode__(self):
        return "%s" % ((self.customer))



class CustomerProfilePicture(models.Model):
    avatar_id = models.CharField(max_length=18, default=rand, unique=True)
    customer = models.ForeignKey("Customer")
    picture_legend = models.CharField(_('Legende'), max_length=128, null=True, blank=True)
    picture_path =  models.CharField("Image de profil", help_text="Image de profil",blank=True, null=True,max_length=128)
    status = models.BooleanField('Status', default=True)
    created_at = models.DateTimeField(_('Date creation'),auto_now_add=True,null=True, blank=True,)
    modified_at= models.DateTimeField('Date modification at ', auto_now=True)

    

    class Meta:
        app_label = 'users'
        verbose_name = _('Customer profile picture')
        verbose_name_plural = _('Customer profile picture')

    def __unicode__(self):
        return "%s " % ((self.picture_path))