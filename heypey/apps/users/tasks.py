from celery import Celery
from celery import shared_task
from django.core.files import File
from users.models import Customer,CustomerProfilePicture
from celery.utils.log import get_task_logger
from django.core.files.storage import FileSystemStorage
from django.core.files.base import ContentFile
import redis
from django.core.files import File 
from django.conf import settings
logger = get_task_logger(__name__)


@shared_task(name='handle_customer_avatar')
def handle_customer_avatar(customer_id,avatar):
    fs = FileSystemStorage()
        
    filename = fs.save(avatar.name, avatar)
    uploaded_file_url = fs.url(filename)
    print uploaded_file_url

    from utils import constants

   
    with open(uploaded_file_url, 'wb+') as dest:
        for chunk in avatar.chunks():
            dest.write(chunk)
    try:
        print uploaded_file_url
        picture = CustomerProfilePicture() 
        picture.picture_path = uploaded_file_url
        picture.customer = Customer.objects.get(id=customer_id)
        picture.save()
    except Exception as e:
        print e
        