"""urlconf for the base application"""

from django.conf.urls import url
from .views import register, login, phone_validation,test, get_customer_info_by_uuid, set_customer_avatar,resend_passcode


urlpatterns = [
    url(r'^register/?$', register,name='register'),
    url(r'^login/$', login,name='login'),
    url(r'^phone_validation/$', phone_validation,name='phone_validation'),
    url(r'^get_customer_info_by_uuid/$', get_customer_info_by_uuid,name='get_customer_info_by_uuid'),
    url(r'^set_customer_avatar/$', set_customer_avatar,name='set_customer_avatar'),
    url(r'^resend_passcode/$', resend_passcode,name='resend_passcode'),
    url(r'^test/$', test,name='test'),
]
