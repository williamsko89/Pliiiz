"""Views for the base app"""

from users.models import Customer, AppUser, CustomerDevice, CustomerGeneratedPassCode\
, CustomerLocationHistory, CustomerProfilePicture
import simplejson
from utils import constants
from utils.ref import code
from utils.rand import rand, decode_base64_file
from django.http import HttpResponse
from django.contrib.gis.geos import Point
from decoupage.models import ReleveZonal
from team.models import TeamActorLocationHistory, TeamActor
from django.views.decorators.csrf import csrf_exempt
from users.tasks import handle_customer_avatar

def test(request):
    return render(request, 'index.html')


@csrf_exempt
def resend_passcode(request):
    try:

        body = simplejson.loads(request.body)
        phone = body['phone']

        #Recherche si le customer existe dja
        try:
            customer = Customer.objects.get(phone = phone)
            passcode = CustomerGeneratedPassCode.objects.filter(customer = customer).last()
            pass_code = passcode.pass_code

            #Send code by SMS as password and register code
        
          
        except Customer.DoesNotExist : 
            result = {
                'success': False,
                'msg':constants.USER_ALREADY_EXISTS,
                'code' : constants.USER_ALREADY_EXISTS,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')

    except KeyError as err:
        print err
        print "KeyError"
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        result = {
            'success': False,
            'msg' : constants.USER_REGISTRATION_ERROR,
            'code' : constants.USER_REGISTRATION_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    result = {
            'success': True,
            'msg': constants.PROCESS_OK,
            'passcode': pass_code
        }

    data = simplejson.dumps(result)
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def register(request):
    try:

        body = simplejson.loads(request.body)
        name = body['name']
        adress = body['address']
        phone = body['phone']
        lon = body['lon']
        lat = body['lat']
        uuid = body['uuid']

        #Recherche si le customer existe dja
        try:
            customer = Customer.objects.get(phone = phone)
            #Utilisateur existe dja ,  Renvoi ERR

            result = {
                'success': False,
                'msg':constants.USER_ALREADY_EXISTS,
                'code' : constants.USER_ALREADY_EXISTS,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')

          
        except Customer.DoesNotExist : 

            pass_code = code()
            #On cree le user ici
            try:
                user = AppUser()
                user.first_name = name
                user.last_name = name
                user.username = phone
                user.set_password(pass_code)
                user.save()
            except Exception as e:
                result = {
                'success': False,
                'msg':constants.USER_ALREADY_EXISTS,
                'code' : constants.USER_ALREADY_EXISTS,
                }
                data = simplejson.dumps(result)
                return HttpResponse(data, content_type='application/json')


            #On cree le customer
            customer = Customer()
            customer.user = user
            customer.phone = phone
            customer.street = adress
            customer.save()

            #TODO
            #On doit rechercher la zone du customer en fonction de lon et lat
            try:
                pt = Point(lon, lat)
                zone = ReleveZonal.objects.get(mpoly__intersects=pt)
                customer.zone = zone.zone
                customer.save()
            except Exception as e:
                print e

                
            #Enregistrer le UUID
            try:
                customer_device = CustomerDevice.objects.get(device_id=uuid)
            except CustomerDevice.DoesNotExist as e:
                customer_device = CustomerDevice()
            
            customer_device.device_id = uuid
            customer_device.customer = customer
            customer_device.save()

            #Send code by SMS as password and register code
            passcode = CustomerGeneratedPassCode()
            passcode.customer = customer
            passcode.pass_code = pass_code
            passcode.save()

            
    except KeyError as err:
        print err
        print "KeyError"
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        print "Exception"
        result = {
            'success': False,
            'msg' : constants.USER_REGISTRATION_ERROR,
            'code' : constants.USER_REGISTRATION_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    result = {
            'success': True,
            'msg': constants.PROCESS_OK,
            'passcode': pass_code
        }

    data = simplejson.dumps(result)
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def login(request):
    """ login """
    try:
        body = simplejson.loads(request.body)
        phone = body['phone']
        lon = body['lon']
        lat = body['lat']
        uuid = body['uuid']

        #Recherche si le customer existe dja
        try:
            pass_code = code()
            user = AppUser.objects.get(username = phone)
            user.set_password(pass_code)
            user.save()

            print user

            customer = Customer.objects.get(user = user)
            #Check AppUser is customer or pliiizer
            is_pliiizer = customer.is_pliiizer
            if is_pliiizer :
                #Loged user is a pliiizer
                pass

            else:
                
                #Enregistrer le UUID
                try:
                    customer_device = CustomerDevice.objects.get(device_id=uuid)
                except CustomerDevice.DoesNotExist as e:
                    customer_device = CustomerDevice()
            
                customer_device.device_id = uuid
                customer_device.customer = customer
                customer_device.save()

            passcode = CustomerGeneratedPassCode()
            passcode.customer = customer
            passcode.pass_code = pass_code
            passcode.save()


        except AppUser.DoesNotExist  as err: 
            print err
            result = {
                'success': False,
                'msg':constants.USER_NOT_FOUND,
                'code' : constants.USER_NOT_FOUND,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')
           
               
            
    except KeyError as err:
        print err
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')


    result = {
        'success': True,
        'msg': constants.PROCESS_OK,
        'passcode': pass_code
    }
    data = simplejson.dumps(result)


    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def phone_validation(request):

    try:
        body = simplejson.loads(request.body)
        phone = body['phone']
        password = body['passcode']
        lon = body['lon']
        lat = body['lat']

        pliiizer_ = None

        #Recherche si le customer existe dja
        try:
            user = AppUser.objects.get(username = phone)
            if user.check_password(password):
                
                #get customer object
                customer = Customer.objects.get(user = user)
                is_pliiizer = customer.is_pliiizer
                print is_pliiizer
                if is_pliiizer:
                    try:
                        print user
                        pliiizer_ = TeamActor.objects.get(user=user)
                        location_history = TeamActorLocationHistory()
                        location_history.team_actor = pliiizer_
                        location_history.location = Point(lon,lat)
                        location_history.save()
                    except Exception as e:
                        print e
                else:
                    try:
                        location_history = CustomerLocationHistory()
                        location_history.customer = customer
                        location_history.location = Point(lon,lat)
                        location_history.save()
                    except Exception as e:

                        print e

            else:
                result = {
                    'success': False,
                    'msg' : constants.INCORRECT_VERFICATION_CODE,
                    'code' : constants.INCORRECT_VERFICATION_CODE,
                }
                data = simplejson.dumps(result)
                return HttpResponse(data, content_type='application/json')
                
        except AppUser.DoesNotExist as err :
            result = {
                'success': False,
                'msg' : constants.USER_NOT_FOUND,
                'code' : constants.USER_NOT_FOUND,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')
          
            #TODO
            #On doit rechercher la zone du customer 
            #en fonction de lon et lat
    except KeyError as err:
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    print "XXXXXX"
    if is_pliiizer:
        pliiizer = pliiizer_.detail()
        print pliiizer
    else :
        pliiizer = None


    print "XXXXXX"
  
    result = {
        'success': True,
        'user': customer.detail(),
        'is_pliiizer':is_pliiizer,
        'pliiizer':pliiizer,
    }

    print result
    print "XXXXXX"
   
    data = simplejson.dumps(result)
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def get_customer_info_by_uuid(request):

    try:
        body = simplejson.loads(request.body)
        uuid = body['uuid']
       
        #Recherche si le customer existe dja
        try:
            user = CustomerDevice.objects.get(device_id = uuid)
            print user
         
                
        except CustomerDevice.DoesNotExist as err :
            result = {
                'success': False,
                'msg' : constants.USER_NOT_FOUND,
                'code' : constants.USER_NOT_FOUND,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')
          
           
    except KeyError as err:
        print err
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')


    result = {
        'success': True,
        'msg':user.customer.detail()
    }
    print result
    data = simplejson.dumps(result)
    return HttpResponse(data, content_type='application/json')



@csrf_exempt
def set_customer_avatar(request):

    print "set_customer_avatar"
    print request

    try:
        body = simplejson.loads(request.body)
        #uuid = '7C751264-572D-4B91-B257-DB184602F5F9'
        uuid = body['uuid']
        try:
            user = CustomerDevice.objects.get(device_id = uuid)
            print user
            if 'avatar' in request.FILES:
                avatar = request.FILES['avatar']
                print avatar
                print handle_customer_avatar.delay(user.customer.id,avatar)
                _data = {'success': True}
                print _data
                data = simplejson.dumps(_data)

        except CustomerDevice.DoesNotExist as err :

            print err
            result = {
                'success': False,
                'msg' : constants.USER_NOT_FOUND,
                'code' : constants.USER_NOT_FOUND,
                }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')

        except Exception as e:
            print e
            result = {
                'success': False,
                'msg' : constants.PROCESSING_ERROR,
                'code' : constants.PROCESSING_ERROR,
            }
            data = simplejson.dumps(result)
            return HttpResponse(data, content_type='application/json')
          
           
    except KeyError as err:
        print err
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')

    except Exception as e:
        print e
        result = {
            'success': False,
            'msg' : constants.KEY_ERROR,
            'code' : constants.KEY_ERROR,
        }
        data = simplejson.dumps(result)
        return HttpResponse(data, content_type='application/json')


    return HttpResponse(data, content_type='application/json')



def get_pliiizer_detail(request):
    body = simplejson.loads(request.body)
    phone = body['phone']
    lon = body['lon']
    lat = body['lat']
    uuid = body['uuid']