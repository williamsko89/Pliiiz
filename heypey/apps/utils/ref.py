#!/usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import division
import rstr

__author__ = "William de SOUZA ♟"


def ref():
    return rstr.digits(9)

def secret():
    return rstr.digits(4)

def code():
    return rstr.digits(6)