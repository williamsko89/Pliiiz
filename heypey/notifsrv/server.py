#! /usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import paho.mqtt.client as mqtt
from datetime import datetime
from unidecode import unidecode
import time, json, requests
app = Flask(__name__)

from django.conf import settings


def json_to_str(data):
    " get str from json without u encode "
    try:
        return json.dumps(data).decode('unicode-escape').encode('utf8')
    except Exception as e:
        print e
    return None


@app.route("/notify/")
def notify():
    try :
        msg =  request.args.get('content')
        channel =  request.args.get('channel')

        print msg
        print channel
        client.publish(channel,msg, qos=1)


        return jsonify(status=True)

    except Exception as e:
        return jsonify(status=False,
            error_msg=str(e)
                   )



# paho callbacks

def on_connect(client, userdata, flags, rc):
    
    #sub here will re subscribe on reconnection
    client.subscribe("PLIIIZ",2)
    print "on_connect"

def on_message(mosq, obj, msg):

    #TODO  ; Check why MQTT sends the message twice
    global message

    message = msg.payload
    data = json.loads(message)

    msg_type = data['msg_type']
    from_user = data['from']
    to_user = data['to']
    message = data['message']
    pliiiz_id = data['pliiiz_id']
    is_pliiizer = data['is_pliiizer']

    print msg_type
    print from_user
    print to_user
    print message
    print pliiiz_id
    print is_pliiizer

    print "XXXX"

    SERVER_URL = '127.0.0.1:8000'
    payload = {'msg_type':msg_type, 'from_user': from_user, 'to_user': to_user, 'message': message, 'is_pliiizer': is_pliiizer, 'pliii_id': pliiiz_id}
    
    r = requests.get(SERVER_URL+'/api/v0/register_chat_msg/', params=payload)





# Main Code
if __name__=='__main__':

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("broker.hivemq.com", 1883, 60)
    client.loop_start()
    client.publish('debug', 'Server running')
    app.run(host = '0.0.0.0' , debug = True, port=9092)



